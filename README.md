# bme280-mqtt

Using a BME280 sensor attached to a RPi3 and MQTT to publish sensor readings

# Development usage

You need Python above version 3.5 and a raspberry pi 3

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/bme-mqtt` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
4. install requirements
4. Run `python3 app.py` (linux) or `py app.py` (windows)

# References
* BME280 library https://pypi.org/project/RPi.bme280/
