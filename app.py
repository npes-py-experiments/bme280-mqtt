import smbus2
import bme280
from socket import error as socket_error
import paho.mqtt.client as mqtt
import time
from datetime import datetime
import random
import json

'''
Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/
created by: Nikolaj Simonsen | february 2021
repo: https://gitlab.com/npes-py-experiments/mqtt-tests 
'''

# variables

# BME280
port = 1
address = 0x77
bus = smbus2.SMBus(port)

calibration_params = bme280.load_calibration_params(bus, address)

# the sample method will take a single reading and return a
# compensated_reading object
#data = bme280.sample(bus, address, calibration_params)

# the compensated_reading class has the following attributes
#print(data.id)
#print(data.timestamp)
#print(data.temperature)
#print(data.pressure)
#print(data.humidity)

# there is a handy string representation too
#print(data)

# paho-mqtt
client_name = 'itt_client_1'
broker_addr = 'test.mosquitto.org'  
broker_port = 1883
topic = 'rpi1/number'
publish_interval = 5

client = mqtt.Client(client_name) #init client

# triggers when message received from broker
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))

# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'connected to {broker_addr} on port {broker_port} ')

# create payload
def build_payload(sensor_reading):
    sensor_id = str(sensor_reading.id)
    timestamp_utc = datetime.utcnow().timestamp() # utc timestamp
    sensor_timestamp = sensor_reading.timestamp.timestamp()
    sensor_temperature = sensor_reading.temperature
    sensor_pressure = sensor_reading.pressure
    sensor_humidity = sensor_reading.humidity
    payload = {'client': client_name, 'topic': topic, 'sensor_id': sensor_id, 'sensor_time': sensor_timestamp, 'temperature': sensor_temperature, 'humidity': sensor_humidity, 'pressure': sensor_pressure}
    return json.dumps(payload) # convert to json format

# read the sensor
def read_sensor():
    sensor_reading = bme280.sample(bus, address, calibration_params)
    return sensor_reading

# attach callbacks
client.on_connect = on_connect # attach callback function
client.on_message = on_message # attach message callback to callback

try: 
    print('connecting to broker')
    client.connect(broker_addr, broker_port)
    print(f'connected to {broker_addr} on port {broker_port} ')
    client.subscribe(topic)
    print(f'Subscribed to topics: {topic}')
    client.loop_start() #start the loop

except socket_error as e:
    print(f'could not connect {client_name} to {broker_addr} on port {broker_port}\n {e}')
    exit(0)

# publish
while(True):
    try:    
        payload = build_payload(read_sensor())
        print(f'Publishing {payload} to topic, {topic}')
        client.publish(topic, payload)        
        time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)
